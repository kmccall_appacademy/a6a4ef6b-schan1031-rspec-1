def translate(x)
  v = ['a','e','i','o','u']
  grp2 = ['qu', 'ch', 'sh', 'th', 'br']
  grp3 = ['squ', 'thr', 'sch']
  t = x.split
  t.map {|w|
    if v.include?(w[0])
      w = w + 'ay'
    elsif grp3.include?(w[0..2])
      w = w[3..-1] + w[0..2] + 'ay'
    elsif grp2.include?(w[0..1])
      w = w[2..-1] + w[0..1] + 'ay'
    else
      w = w[1..-1] + w[0] + 'ay'
    end
  }.join(' ')
end

