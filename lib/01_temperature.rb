def ftoc(tmp)
  return (tmp-32)*5/9
end

def ctof(tmp)
  return (tmp.to_f)*9/5 + 32
end

