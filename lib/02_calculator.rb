def add(n1,n2)
  return n1 + n2
end

def subtract(n1,n2)
  return n1 - n2
end

def sum(arr)
  if arr.empty?
    return 0
  else
    return arr.reduce(&:+)
  end
end
