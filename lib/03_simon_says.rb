def echo(x)
  return x
end

def shout(x)
  return x.upcase
end

def repeat(x,n = nil)
  if n.nil?
    x + ' ' + x
  else
    [x] * n * ' '
  end
end

def start_of_word(x,n)
  x[0..n-1]
end

def first_word(x)
  x.split[0]
end

def titleize(x)
  littles = ["and", "or", "the", "over", "The"]
  x.capitalize.split.map {|w| 
    if littles.include?(w)
      w
    else
      w.capitalize
    end
  }.join(' ')
end

